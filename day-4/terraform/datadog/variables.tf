variable "DD_API_KEY" {
  type = string
}

variable "DD_APP_KEY" {
  type = string
}

variable "api_url" {
  type = string
  default = "https://api.datadoghq.com"
}
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  owners = ["099720109477"] #Ubuntu
}

resource "aws_instance" "create_ec2" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.ec2_instance_type
  key_name      = var.ec2_keypair_name
  count         = var.ec2_instance_count

  tags = {
    Name = var.ec2_tags
  }
}

output "instance_ip" {
  value = aws_instance.create_ec2[*].public_ip
}

output "instance_private_ip" {
  value = aws_instance.create_ec2[*].private_ip
}
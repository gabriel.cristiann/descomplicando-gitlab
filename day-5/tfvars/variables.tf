variable "ec2_region" {
  type    = string
  default = "us-east-2"
}

variable "ec2_keypair_name" {
  type    = string
  default = "gitlab-server-key"
}

variable "ec2_instance_type" {
  type    = string
  default = "t2.micro"
}

variable "ec2_tags" {
  type    = string
  default = "Descomplicando Gitlab"
}

variable "ec2_instance_count" {
  type    = number
  default = 1
}